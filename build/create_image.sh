t#!/bin/bash
# File :create_image.sh
# Author : Chandrasekaran "<chandrasekaran@thebw.in>"
# Description: Creates Syclops Docker images.
#
SYCLOPS_TRM="syclops/syclops_term"
TERMINAL_SRC="./terminal/src/gateone"
#
# User Validation
#
if [[ $(whoami) != 'root' ]];then
    echo "You are not authorized user to run this script. Please try as a sudo user"
    exit 1
fi
#
# Installs Docker latest version
#
docker_status=$(type -P docker &>/dev/null && echo 1 || echo "")
if [[ -z $docker_status ]]; then
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    apt-get update
    apt-cache policy docker-ce
    apt-get install -y docker-ce
    usermod -aG docker ${USER}
    echo "$(docker version) installation success."
fi
#
# Checks Docker installation status
#
if [[ $? != 0 ]];then
    echo "Docker installation faild"
    exit 1
fi
#
# Checks  Syclops Terminal source file
#
if [[ ! -d $TERMINAL_SRC ]];then
    echo "Download Terminal source and Syclops terminal plugin and put it in this location ./terminal/src/gateone"
    exit 0
fi

if [[ -d ./terminal ]];then
    if [[ -d $TERMINAL_SRC ]];then
        docker build -t $SYCLOPS_TRM ./terminal
    else
        echo "Terminal source is not found in this location $TERMINAL_SRC"
        echo "Please download terminal source code and then continue"
        exit 1
    fi
 else
    echo "terminal folder is not found"
    exit 1
fi
