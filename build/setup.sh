#!/bin/bash
#Creates required directory for backup
if [[ ! -f /syclops ]];then
  mkdir -p /syclops/terminal
fi

#Downloads gateone source and plugin
if [[ ! -f ./terminal/src/gateone ]]; then
  #Downloads Gateone source
  cd ./terminal/src
  git clone https://bitbucket.org/syclops/syclops_gateone.git gateone
  #Downloads Gateone Plugin
  cd gateone/gateone/applications/terminal/plugins
  git clone https://bitbucket.org/syclops/syclops_gateone_plugin.git syclops
  cd -
fi
